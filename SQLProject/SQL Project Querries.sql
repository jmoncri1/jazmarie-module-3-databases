SELECT * FROM Address;
SELECT * FROM Customers;
SELECT * FROM Inventory;
SELECT * FROM Items;
SELECT * FROM Payment_types;
SELECT * FROM Purchase_items;
SELECT * FROM Purchases;
SELECT * FROM Stores;
 /*1. As an owner, I want a query to view the following information from all stores, so
that I can remember where all my stores are (ordered by store name):
1. Store name
2. Store address
*/
SELECT DISTINCT Stores.name AS "Store Name", CONCAT(Address.street, ', ', Address.street2, ', ', Address.city, ', ', Address.state, ', ', Address.zip) 
AS "Address"
FROM Stores, Address
WHERE Address.street2 IS NOT NULL;

/*2. As an auditor, I want a query to view the following information from all stores, so
that I can quickly view sales per store (ordered by store name):
1. Store name
2. Total sales in dollars
*/
SELECT DISTINCT Stores.name AS "Store Name", SUM(Items.price * Purchase_items.quantity) AS "Total Sales in Dollars"
FROM Stores, Items, Purchase_items, Purchases
WHERE Purchases.store_id = Stores.id
GROUP BY Stores.name;

/*3. As a marketing director, I want a query to view the following information from all
stores, so that I can provide targeted ad campaigns to specific individuals
(ordered by store name):
1. Store name
2. Member name
*/
SELECT DISTINCT Stores.name AS "Store Name",  STRING_AGG(Customers.last_name || ', ' || first_name, e'\n') AS "Customer Name"
FROM Stores
LEFT JOIN Customers
ON Customers.store_id = Stores.id
GROUP BY Stores.name;

/*4. As the store manager of the Administaff, Inc., I want a query to view the following
information from my specific store (only information about Administaff), so that I
can quickly see my store's inventory (ordered by item name):
1. Store name
2. Item name
3. Quantity in this store
4. Price per unit
5. Notes about the unit
*/
SELECT DISTINCT Stores.name as "Store Name", 
Items.name AS "Item Name", 
Purchase_items.quantity AS "Quantity", 
Items.price AS "Price Per Unit",
Items.notes AS "Notes"
FROM Items, Stores, Purchase_items, Purchases
WHERE Stores.id = Purchases.store_id AND Items.id = purchase_items.item_id AND Stores.name = 'Administaff, Inc.'
ORDER BY Items.name;

/*5. As the store manager of the Administaff, Inc., I want a query to view the following
information from my specific store (only information about Administaff), so that I
can request additional inventory before the high dollar sales months (ordered in
ascending month):
1. Month of purchase
2. Total sales
*/

SELECT EXTRACT(MONTH FROM Purchases.purchase_date) AS "Month of Purchase",
SUM(Items.price * Purchase_items.quantity) AS "Total Sales"
FROM Purchases, Items, Purchase_items, Stores
WHERE Purchase_items.purchase_id = Purchases.id AND Stores.name = 'Administaff, Inc.'
GROUP BY EXTRACT(MONTH FROM Purchases.purchase_date)
ORDER BY EXTRACT(MONTH FROM Purchases.purchase_date)

/*6. As an auditor who is currently auditing Benchmark Electronics, Inc., I want a
query to view the following information about Benchmark Electronics, Inc. so that
I can verify that those store managers are properly reporting cash payments
(ordered by Payment Method, must be the payment method name, not its ID):
1. Store name
2. Payment method
3. Count of purchases by payment method
*/
SELECT Stores.name AS "Store Name", 
Payment_types.payment_method AS "Payment Method", 
SUM(Purchase_items.purchase_id)
FROM Stores, Payment_types, Purchase_items, Purchases
WHERE Purchases.payment_type_id = Payment_types.id 
AND Purchases.store_id = Stores.id 
AND Stores.name = 'Benchmark Electronics, Inc.'
GROUP BY Stores.name, Payment_types.payment_method
ORDER BY Payment_types.payment_method

/*7. As a regional inventory manager, I want a query to view low inventory (Fewer than
10) items by store, so that I can ship out more items to stores that need them
(ordered by store name):
1. Store Name
2. Item Name
3. Quantity of Item in that store
*/
SELECT DISTINCT Stores.name AS "Store Name",
Items.name AS "Item Name",
Inventory.quantity AS "Quantity"
FROM Stores
JOIN Inventory ON Inventory.store_id = Stores.id
JOIN Items ON Inventory.item_id = Items.id
GROUP BY Stores.name, Items.name, Inventory.quantity
ORDER BY Stores.name

/*8. A customer named Alfred Poggi has made a complaint about an item they
bought. As a sales representative in a returns department, I want a query to see
Alfred’s purchase history (ordered by purchase date, newest to oldest):
1. Member’s first and last name
2. Purchase date
3. Name of item purchased - If there are multiple items in a single purchase,
please retrieve a single record per item name
4. The name of the payment type they used
5. Quantity of items purchased
*/
SELECT CONCAT(Customers.first_name || ', ' || last_name) AS "Customer Name",
Purchases.purchase_date AS "Purchase Date",
Items.name AS "Name of Item",
Payment_types.payment_method AS "Payment Type",
Purchase_items.quantity AS "Quantity"
FROM Customers
JOIN Purchases ON Purchases.customer_id = Customers.id
JOIN Purchase_items ON Purchase_items.purchase_id = Purchases.id
JOIN Items ON Items.id = Purchase_items.item_id
JOIN Payment_types ON Payment_types.id = Purchases.payment_type_id

/*9. The chain is discontinuing its support for Apple Pay, and will treat prior Apple Pay
purchases as credit purchases. As a corporate manager, I want a query to update
all of our past purchases that used Apple Pay to become purchases made using
Credit.
*/
UPDATE Purchases
SET payment_type_id = (SELECT id FROM payment_types WHERE payment_method = 'credit' )
WHERE payment_type_id = (SELECT id FROM payment_types WHERE payment_method = 'apple pay');


/*10.As a corporate manager, I want to remove Apple Pay from the list of payment
types as it is no longer supported by our company.
*/
DELETE FROM Payment_types
WHERE payment_method = 'apple pay';



/*11.As a marketing director, I want a query that will add a new item to our stores.
1. Item details:
1. Name - “Frosted Flakes”
2. Price - 5
3. Notes - “They’re Grate!”
*/
INSERT INTO items (name, price, notes)
VALUES ('Frosted Flakes', 5, 'They''re Grate!');

/*12.As a regional inventory manager, I want a single query to add 50 of the new
Frosted Flakes items to every store with a name containing the letter “a”.
*/

INSERT INTO inventory (store_id, item_id, quantity)
SELECT Stores.id, Items.id, 50
FROM stores, items
WHERE stores.name ILIKE 'a%' AND items.name = 'Frosted Flakes';

/*13.As a corporate manager, I want to fix a mistake that our marketing director made.
I want a query that will update the notes on one of our products:
1. Product Name: “Frosted Flakes”
2. Incorrect (current) Notes - “They’re Grate!”
3. Correct Notes (please update to this) - “They’re Gr-r-reat!”
*/
UPDATE items
SET notes = 'They''re Gr-r-reat!'
WHERE items.name = 'Frosted Flakes'

