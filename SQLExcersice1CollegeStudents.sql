SELECT * FROM classes;
SELECT * FROM grades;
SELECT * FROM students;

/*1. A new student has been accepted into the college, 
add his information to the students table: Dan Zanes. */

INSERT INTO students
VALUES (2, 'Dan', 'Zanes', 'dzanes', 'dzanes@college.edu');

/*2. Josie’s email was inputted incorrectly. Modify her email to be
“jblair21@college.edu”.*/

UPDATE students
SET email = 'jblair21@college.edu'
WHERE id = 1;

/*3. The college is creating a directory of all student names and emails. To help,
generate a list of all students in the format: <Last name, First name> <Email>
with a heading of “Name” for the full name and “Email” for the email. Order the
list alphabetically by last name.*/

SELECT last_name || ', ' || first_name AS "Name", email AS "Email"
FROM students
ORDER BY last_name ASC;

/*4. Abe wants to see all of his recorded class grades. Create a list for him with the
name of the class and the grade he received.*/

SELECT classes.name, grades.grade
FROM classes
JOIN grades ON classes.id=grades.class_id
WHERE grades.student_id = 3;

/*5. Joe Barnes has graduated and his information has been recorded in an alumni
table. Delete his record from the students table.*/

DELETE FROM grades
WHERE student_id = 4;

DELETE FROM students
WHERE last_name = 'Barnes';


/*6. The teacher of Introduction to Computer Science wants to see a list of all the grades in the class. 
Create a list with the full name of the student and the grade received in Introduction to Computer Science.*/

SELECT Classes.name AS "Class", last_name || ', ' || first_name AS "Name", grades.grade AS "Grades"
FROM Grades
JOIN Students ON Students.id = Grades.student_id

/*STRETCH
1. An administrator would like to see a list of all grades in all classes for all
students. Create a list which shows all of the grades, the class name, and the
student’s full name.*/

SELECT grade AS "Grades", Classes.name AS "Class", last_name || ', ' || first_name AS "Student Name"
FROM Grades
JOIN Students ON Students.id = student_id
JOIN Classes ON class_id = Classes.id
JOIN Classes ON Grades.class_id = Classes.id
WHERE Classes.name ILIKE '%computer%';






